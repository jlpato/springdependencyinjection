package com.praxis.curso.springdi.app;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.praxis.curso.springdi.configuration.DIConfiguration;
import com.praxis.curso.springdi.main.MyApplication;

public class ClientApplication {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DIConfiguration.class);
		MyApplication app = context.getBean(MyApplication.class);
		
		app.processMessage("Hi Pankaj", "pankaj@abc.com");
		
		//close the context
		context.close();
	}

}
