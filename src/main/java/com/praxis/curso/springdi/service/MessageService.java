package com.praxis.curso.springdi.service;

public interface MessageService {

	boolean sendMessage(String msg, String rec);

}
