package com.praxis.curso.springdi.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.praxis.curso.springdi.service.EmailServiceImpl;
import com.praxis.curso.springdi.service.MessageService;

@Configuration
@ComponentScan(value={
		"com.praxis.curso.springdi.service",
		"com.praxis.curso.springdi.main"
	})
public class DIConfiguration {

	@Bean
	public MessageService getMessageService(){
		return new EmailServiceImpl();
	}

}
